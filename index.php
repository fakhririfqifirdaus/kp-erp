<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="css/bootstrap.css" />

	<!-- Font Awesome -->
	<link rel="stylesheet" href="css/all.css" />

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap"
		rel="stylesheet" />

	<!-- My CSS -->
	<link rel="stylesheet" href="css/style.css" />

	<title>ERP Laboratory</title>
</head>

<body>
	<!-- Navbar -->
	<nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light">
		<div class="container">
			<a class="navbar-brand" href="index.php">
				<img src="img/erp-logo.png" alt="ERP Laboratory" />
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
				aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavDropdown">
				<ul class="navbar-nav text-uppercase mx-auto">
					<li class="nav-item">
						<a class="nav-link" href="pages/tentang-kami.php">Tentang Kami</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Event
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="pages/event-berita.php">Berita Terbaru</a>
							<a class="dropdown-item" href="pages/info-kegiatan.php">Info Kegiatan</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Repository
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="pages/unggah-abstrak.php">Unggah Abstrak</a>
							<a class="dropdown-item" href="pages/database-alumni.php">Database Alumni</a>
							<a class="dropdown-item" href="pages/cek-plagiarisme.php">Cek Plagiarisme</a>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Lowongan</a>
					</li>
				</ul>
				<a href="" class="nav-link">MASUK</a>
				<a href="" class="nav-link">DAFTAR</a>
			</div>
		</div>
	</nav>
	<!-- Akhir Navbar -->

	<!--Main Content  -->
	<!-- Main Content 1 -->
	<div class="container my-5">
		<div class="row">
			<div class="col-md-7 col-sm-12">
				<div class="about-info">
					<h3><strong>Selamat Datang di ERP Research Center</strong></h3><br>

					<p align="justify">Laboratorium Enterprise Resource Planning atau Lab ERP merupakan salah satu
						laboratorium yang berada dibawah naungan program studi Sistem Informasi, Fakultas Rekayasa
						Industri, Universitas Telkom.</p>

					<p align="justify">Laboratorium ERP didirikan pada tahun 2008 yang pada awalnya bernama Business
						Information System Laboratory (BIS Laboratory). Kemudian pada tahun 2011, BIS Laboratory
						dipecah menjadi 2 Laboratorium dengan 2 bidang fokus yang berbeda
						yaitu Laboratorium ERP dan Laboratorium BPAD (Business Process Analysis and Design).</p>

					<p align="justify">Laboratorium ERP merupakan wadah akademik yang berfokus kepada bidang
						Kelompok Keahlian ESA khususnya bidang ERP atau Enterprise Resource Planning. Laboratorium
						ERP menangani 4 praktikum matakuliah beserta ujian sertifikasinya
						meliputi SAP 01 Fundamental, SAP SCM100 dan SCM300, SAP HR050, dan SAP AC010. Selain itu,
						Laboratorium ERP juga turut berfokus pada kegiatan lain seperti Perlombaan, Pengabdian
						Masyarakat, Riset terkait ERP dan juga Project.
					</p>

				</div>
			</div>

			<div class="col-md-5 col-sm-12 my-auto">
				<div class="card fadeInUp about-image animated" data-wow-delay="0.6s"
					style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
					<img src="/img/erpasisstant 1.png" class="img-responsive" alt="" />
				</div>
			</div>
		</div>
	</div>
	<!-- Akhir Main Content 1 -->

	<!-- Main Content 2 -->
	<div class="container my-4 mx-auto">
		<h3 align="center">Layanan Kami</h3>
		<div class="card-deck mt-4">
			<div class="card" data-clickable="true" data-href="pages/praktikum.php">
				<img class="card-img-top m-auto" style="width: 300px; height: 250px;" src="img/praktikum.png"
					alt="Card image cap" />
				<div class="card-body">
					<h5 class="card-title text-center font-weight-bold">Praktikum</h5>
				</div>
			</div>
			<div class="card" data-clickable="true" data-href="pages/riset.php">
				<img class="card-img-top m-auto" style="width: 300px; height: 250px;" src="img/riset.png"
					alt="Card image cap" />
				<div class="card-body">
					<h5 class="card-title text-center font-weight-bold">Riset</h5>
				</div>
			</div>
			<div class="card" data-clickable="true" data-href="pages/lomba.php">
				<img class="card-img-top m-auto" style="width: 300px; height: 250px;" src="img/lomba.png"
					alt="Card image cap" />
				<div class="card-body">
					<h5 class="card-title text-center font-weight-bold">Lomba</h5>
				</div>
			</div>
		</div>

		<div class="card-deck mt-5 mb-5">
			<div class="card" data-clickable="true" data-href="pages/pengabdian-masyarakat.php">
				<img class="card-img-top m-auto" style="width: 300px; height: 250px;" src="img/pengmas.png"
					alt="Card image cap" />
				<div class="card-body">
					<h5 class="card-title text-center font-weight-bold">Pengabdian Masyarakat</h5>
				</div>
			</div>
			<div class="card" data-clickable="true" data-href="pages/project.php">
				<img class="card-img-top m-auto" style="width: 300px; height: 250px;" src="img/project.png"
					alt="Card image cap" />
				<div class="card-body">
					<h5 class="card-title text-center font-weight-bold">Project</h5>
				</div>
			</div>
			<div class="card" data-clickable="true" data-href="pages/pelatihan.php">
				<img class="card-img-top m-auto" style="width: 300px; height: 250px;" src="img/pelatihan.png"
					alt="Card image cap" />
				<div class="card-body">
					<h5 class="card-title text-center font-weight-bold">Pelatihan</h5>
				</div>
			</div>
		</div>
	</div>
	<!-- Akhir Main Content 2 -->
	<!-- Akhir Main Content -->

	<!-- Awal Footer -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col">
					<a href="">
						<img src="img/erp-logo.png" alt="ERP Laboratory" />
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<p style="color: white; margin-bottom: 0px;">C226 Building Telkom Engineering School,
						Jl.Telekomunikasi No. 1 Terusan Buah Batu <br> Dayeuh Kolot, Bandung</p>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<a href="">
						<img src="img/pin.png" alt="" style="margin-right: 10px;" />
					</a>
					<a href="">
						<img src="img/ig.png" alt="" style="margin-right: 10px;" />
					</a>
					<a href="">
						<img src="img/yt.png" alt="" style="margin-right: 10px;" />
					</a>
					<a href="">
						<img src="img/fb.png" alt="" style="margin-right: 10px;" />
					</a>
					<a href="">
						<img src="img/li.png" alt="" style="margin-right: 10px;" />
					</a>
				</div>
			</div>
			<div class="row border-top mt-3">
				<div class="col">
					<p style="color: white; margin-bottom: 0px;">2020 ERP Laboratory<br>Managed By Public Relation ERP
						Lab | Developed By</p>
				</div>
			</div>
		</div>
	</footer>
	<!-- Akhir Footer -->

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="js/jquery-3.5.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/all.js"></script>
	<script>
		$(document).ready(() => {
			$('.card[data-clickable=true]').click(function (e) {
				var href = $(e.currentTarget).data('href')
				console.log(href)
				window.location = href
			})
		})
	</script>
</body>

</html>