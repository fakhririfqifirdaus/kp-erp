<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.css" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="/css/all.css" />

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap"
        rel="stylesheet" />

    <!-- My CSS -->
    <link rel="stylesheet" href="/css/style.css" />

    <title>ERP Laboratory</title>
</head>

<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="/index.php">
                <img src="/img/erp-logo.png" alt="ERP Laboratory" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav text-uppercase mx-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/pages/tentang-kami.php">Tentang Kami</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Event
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item active" href="/pages/event-berita.php">Berita Terbaru</a>
                            <a class="dropdown-item" href="/pages/info-kegiatan.php">Info Kegiatan</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Repository
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Unggah Abstrak</a>
                            <a class="dropdown-item" href="#">Database Alumni</a>
                            <a class="dropdown-item" href="#">Cek Plagiarisme</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Lowongan</a>
                    </li>
                </ul>
                <a href="" class="nav-link">MASUK</a>
                <a href="" class="nav-link">DAFTAR</a>
            </div>
        </div>
    </nav>
    <!-- Akhir Navbar -->

    <!--Main Content  -->
    <div class="header">
        <div class="container my-5">
            <h3>Berita Terbaru</h3>
        </div>
    </div>
    <!-- Main Content 1 -->
    <div class="container my-5">
        <div class="row">
            <div class="col-md-8 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h3><b>JUARA III LOMBA APPLICATION DEVELOPMENT - INFOVEST POLITEKNIK HARAPAN BERSAMA TEGAL
                                2018</b></h3>
                        <img src="http://erplab.id/po-content/uploads/191241.jpg" class="card-img-top" alt="foto">
                        <p></p>
                        <p>
                            ERP mengucapkan selamat kepada :

                        </p>
                        <p> 1.Anantya Khrisna S (ERP 2015)</p>
                        <p> 2. Taufan Fadhilah I (EAD 2015)</p>
                        <p> 3. Niken Febriani K (EAD 2015)</p>
                        <p> 4. M Yusuf Matra (SI 2015)</p>

                        <p> Telah meraih Juara 3 pada lomba INFOVEST 2018 kategori Application Development Competition
                            yang diselenggarakan di Politeknik Harapan Bersama Tegal.

                            Semoga dengan diraihnya Juara 3 ini dapat memberikan motivasi kepada para mahasiswa lainnya
                            untuk membanggakan jurusan Sistem Informasi.

                            #ERPLabSIBanget
                            #JanganKasihKendor
                            #JanganLupaBahagia
                            #ForTheFuture
                        </p>
                    </div>
                </div>
            </div>



            <div class="col-md-4 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h4><b>Berita Terkait</b></h4>
                        <div class="card" style="width: 18rem;">
                            <img src="http://erplab.id/po-content/uploads/191289.jpg" class="card-img-top" alt="foto">
                            <div class="card-body">
                                <h5 class="card-title">JUARA III LOMBA INTENTION ANDROID APPS DEVELOPMENT</h5>
                                <p class="card-text">ERP Laboratory mengucapkan selamat kepada :…</p>
                                <a href="#"><u> Selengkapnya</u> </a>
                            </div>
                        </div>
                        <p></p>
                        <p></p>
                        <div class="card" style="width: 18rem;">
                            <img src="http://erplab.id/po-content/uploads/191293.jpg" class="card-img-top" alt="foto">
                            <div class="card-body">
                                <h5 class="card-title">JUARA II LOMBA BUSINESS IT CASE - IT TODAY INSTITUT PERTANIAN
                                    BOGOR 2018</h5>
                                <p class="card-text">ERP Laboratory mengucapkan selamat kepada :…</p>
                                <a href="#"><u> Selengkapnya</u> </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Akhir Main Content 1 -->
    <!-- Akhir Main Content -->

    <!-- Awal Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="">
                        <img src="/img/erp-logo.png" alt="ERP Laboratory" />
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p style="color: white; margin-bottom: 0px;">C226 Building Telkom Engineering School,
                        Jl.Telekomunikasi No. 1 Terusan Buah Batu <br> Dayeuh Kolot, Bandung</p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <a href="">
                        <img src="/img/pin.png" alt="" style="margin-right: 10px;" />
                    </a>
                    <a href="">
                        <img src="/img/ig.png" alt="" style="margin-right: 10px;" />
                    </a>
                    <a href="">
                        <img src="/img/yt.png" alt="" style="margin-right: 10px;" />
                    </a>
                    <a href="">
                        <img src="/img/fb.png" alt="" style="margin-right: 10px;" />
                    </a>
                    <a href="">
                        <img src="/img/li.png" alt="" style="margin-right: 10px;" />
                    </a>
                </div>
            </div>
            <div class="row border-top mt-3">
                <div class="col">
                    <p style="color: white; margin-bottom: 0px;">2020 ERP Laboratory<br>Managed By Public Relation ERP
                        Lab | Developed By</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Akhir Footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/js/jquery-3.5.1.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/all.js"></script>
    <script>
        $(document).ready(() => {
            $('.card[data-clickable=true]').click(function (e) {
                var href = $(e.currentTarget).data('href')
                console.log(href)
                window.location = href
            })
        })
    </script>
</body>

</html>