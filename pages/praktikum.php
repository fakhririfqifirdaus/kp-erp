<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.css" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="/css/all.css" />

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap"
        rel="stylesheet" />

    <!-- My CSS -->
    <link rel="stylesheet" href="/css/style.css" />

    <title>ERP Laboratory</title>
</head>

<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light">
		<div class="container">
			<a class="navbar-brand" href="/index.php">
				<img src="/img/erp-logo.png" alt="ERP Laboratory" />
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
				aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavDropdown">
				<ul class="navbar-nav text-uppercase mx-auto">
					<li class="nav-item">
						<a class="nav-link" href="/pages/tentang-kami.php">Tentang Kami</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Event
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="/pages/event-berita.php">Berita Terbaru</a>
							<a class="dropdown-item" href="/pages/info-kegiatan.php">Info Kegiatan</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Repository
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="#">Unggah Abstrak</a>
							<a class="dropdown-item" href="#">Database Alumni</a>
							<a class="dropdown-item" href="#">Cek Plagiarisme</a>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Lowongan</a>
					</li>
				</ul>
				<a href="" class="nav-link">MASUK</a>
				<a href="" class="nav-link">DAFTAR</a>
			</div>
		</div>
	</nav>
    <!-- Akhir Navbar -->

    <!--Main Content  -->
    <!-- Main Content 1 -->
    <div class="container my-5">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="about-info">
                    <div class="section-title wow fadeInUp animated" data-wow-delay="0.2s"
                        style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <h3><strong>Praktikum</strong></h3>
                        <br>
                    </div>

                    <class class="wow fadeInUp animated" data-wow-delay="0.4s"
                        style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                        <p align="justify">Praktikum merupakan komponen pendidikan yang digunakan untuk mengasah
                            kemampuan hardskill mahasiswa dan mengimplementasikan teori-teori yang dipelajari dikelas
                            kedalam kasus-kasus di dunia nyata.</p>

                        <p>Di Laboratorium ERP, Kami mengampu 4 praktikum utama diantaranya :</p>

                        <hr>
                        <p align="justify">
                            <strong>Praktikum Sistem Enterprise</strong>

                            <p>Praktikum sistem enterprise merupakan praktikum yang diselenggarakan untuk mahasiswa
                                semester 1 yang bertujuan untuk mengajarkan mahasiswa sistem informasi tuntuk mengenal
                                dasar
                                dasar proses bisnis dan mengimplementasikannya kedalam sistem yang terintegrasi.</p>

                            <p>Praktikum ini diampu dengan menggunakan modul SAP01 Fundamental. Akhir dari praktikum ini
                                adalah sertifikasi SAP01 Fundamental.</p>
                        </p>

                        <hr>
                        <p align="justify">
                            <strong>Praktikum Manajemen Rantai Pasok</strong>

                            <p>Praktikum Manajemen Rantai Pasok (SCM) merupakan praktikum tingkat lanjut yang ditujukan
                                untuk mahasiswa Sistem Informasi semester 4 yang bertujuan untuk mengenalkan teori
                                konsep Rantai Pasok yang telah dipelajari dikelas dan diimplementasikan dalam sistem
                                terintegrasi dari mulai perencanaan hingga akhir proses produksi sehingga mahasiswa
                                mendapat gambaran konsep tersebut secara real.</p>

                            <p>Praktikum ini diampu dengan menggunakan modul SCM100 (Processes in Planning) dan SCM300
                                (Production Overview). Akhir dari praktikum ini adalah sertifikasi SAP SCM100 dan SAP
                                SCM300.</p>
                        </p>
                        <hr>
                        <p align="justify">
                            <strong>Praktikum Sistem dan Manajemen Sumber Daya Manusia</strong>

                            <p>Praktikum Sistem dan Manajemen Sumber Daya Manusian (SMSDM), merupakan praktikum tingkat
                                lanjut yang ditujukan untuk mahasiswa sistem informasi semester 5 yang bertujuan untuk
                                memberi gambaran kepada mahasiswa tentang bagaimana cara mengelola SDM disuatu perusahan
                                dengan menggunakan sebuah sistem yang terintegrasi.</p>

                            <p>Praktikum ini diampu dengan menggunakan modul HR050 (Business Process in Human Capital
                                Management). Akhir dari praktikum ini adalah sertifikasi SAP HR050.</p>
                        </p>
                        <hr>
                        <p align="justify">
                            <strong>Praktikum Sistem Akuntansi dan Manajemen Keuangan</strong>

                            <p>Praktikum Sistem Akuntansi dan Manajemen Keuangan, merupakan praktikum tingkat lanjut
                                yang ditujukan untuk mahasiswa sistem informasi semester 6 yang bertujuan untuk memberi
                                gambaran dan mempraktikkan kepada mahasiswa tentang bagaimana cara mengelola keuangan
                                dan transaksi finansial disuatu perusahan dengan menggunakan sebuah sistem yang
                                terintegrasi.</p>

                            <p>Praktikum ini diampu dengan menggunakan modul AC010 (Business Process in Financial
                                Accounting). Akhir dari praktikum ini adalah sertifikasi SAP AC010. </p>
                        </p>
                        </class>
                </div>
            </div>
        </div>
    </div>
    <!-- Akhir Main Content 1 -->
    <!-- Akhir Main Content -->

    <!-- Awal Footer -->
    <footer>
		<div class="container">
			<div class="row">
				<div class="col">
					<a href="">
						<img src="/img/erp-logo.png" alt="ERP Laboratory" />
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<p style="color: white; margin-bottom: 0px;">C226 Building Telkom Engineering School,
						Jl.Telekomunikasi No. 1 Terusan Buah Batu <br> Dayeuh Kolot, Bandung</p>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<a href="">
						<img src="/img/pin.png" alt="" style="margin-right: 10px;" />
					</a>
					<a href="">
						<img src="/img/ig.png" alt="" style="margin-right: 10px;" />
					</a>
					<a href="">
						<img src="/img/yt.png" alt="" style="margin-right: 10px;" />
					</a>
					<a href="">
						<img src="/img/fb.png" alt="" style="margin-right: 10px;" />
					</a>
					<a href="">
						<img src="/img/li.png" alt="" style="margin-right: 10px;" />
					</a>
				</div>
			</div>
			<div class="row border-top mt-3">
				<div class="col">
					<p style="color: white; margin-bottom: 0px;">2020 ERP Laboratory<br>Managed By Public Relation ERP
						Lab | Developed By</p>
				</div>
			</div>
		</div>
	</footer>
    <!-- Akhir Footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/js/jquery-3.5.1.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/all.js"></script>
    <script>
        $(document).ready(() => {
            $('.card[data-clickable=true]').click(function (e) {
                var href = $(e.currentTarget).data('href')
                console.log(href)
                window.location = href
            })
        })
    </script>
</body>

</html>