<?php
include('config_database.php');
// mengecek apakah di url ada nilai GET id
if (isset($_GET['id_repo'])) {
  // ambil nilai id dari url dan disimpan dalam variabel $id
  $id = ($_GET["id_repo"]);

  // menampilkan data dari database yang mempunyai id=$id
  $query = "SELECT * FROM lowongan WHERE id_repo='$id'";
  $result = mysqli_query($koneksi, $query);
  // jika data gagal diambil maka akan tampil error berikut
  if (!$result) {
    die("Query Error: " . mysqli_errno($koneksi) .
      " - " . mysqli_error($koneksi));
  }
  // mengambil data dari database
  $data = mysqli_fetch_assoc($result);
  // apabila data tidak ada pada database maka akan dijalankan perintah ini
  if (!count($data)) {
    echo "<script>alert('Data tidak ditemukan pada database');window.location='index.php';</script>";
  }
} else {
  // apabila tidak ada data GET id pada akan di redirect ke index.php
  echo "<script>alert('Masukkan data id.');window.location='index.php';</script>";
}

?>

<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="/css/bootstrap.css" />

  <!-- Font Awesome -->
  <link rel="stylesheet" href="/css/all.css" />

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet" />

  <!-- My CSS -->
  <link rel="stylesheet" href="/css/style.css" type="text/css" />

  <title>Info Lowongan</title>
</head>

<body>
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="/index.html">
        <img src="/img/erp-logo.png" alt="ERP Laboratory" />
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav text-uppercase mx-auto">
          <li class="nav-item">
            <a class="nav-link" href="/pages/tentang-kami.html">Tentang Kami</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Event
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="/event/berita-terbaru.html">Berita Terbaru</a>
              <a class="dropdown-item" href="/event/info-kegiatan.html">Info Kegiatan</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Repository
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="unggah-abstrak.html">Unggah Abstrak</a>
              <a class="dropdown-item" href="database-alumni.html">Database Alumni</a>
              <a class="dropdown-item" href="cek-plagiarisme.html">Cek Plagiarisme</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="lowongan.html">Lowongan</a>
          </li>
        </ul>
        <a href="" class="nav-link">MASUK</a>
        <a href="" class="nav-link">DAFTAR</a>
      </div>
    </div>
  </nav>
  <!-- Akhir Navbar -->
  <div class="row">
    <img src="gambar/<?php echo $data['gambar']; ?>" class="foto">

  </div>
  <h3 class="inflan">Informasi Lain</h4>
    <h3 class="infum">Informasi Umum</h4>
      <h4 class="company">Pengarang : <?php echo $data['pengarang']; ?></h4>
      <h5 class="jobdes"> Penerbit : <?php echo $data['penerbit']; ?></h5>
      <h6 class="sirkul">Sirkulasi :</h6>
      <h6 class="download">Download</h6>
      <p class="des">
        <b>A. Letter of Acceptance (Publish External Only) (letter_of_acceptance.pdf)
          <br><a href="#" class="read-more">Download</a>
          <br><b>B. Cover (cover.pdf)
            <br><a href="#" class="read-more">Download</a>
            <br><b>C. Disclaimer (Pernyataan Orisinalitas) yang sudah bertandatangan (disclaimer.pdf)
              <br><a href="#" class="read-more">Download</a>
              <br><b>D. Lembar Pengesahan yang sudah bertandatangan (lembarpersetujuan.pdf)
                <br><a href="#" class="read-more">Download</a>
                <br><b>E. Abstrak ( Indonesia ) (abstraksi.pdf)
                  <br><a href="#" class="read-more">Download</a>
                  <br><b>F. Abstract (English) (abstract.pdf)
                    <br><a href="#" class="read-more">Download</a>
                    <br><b>H. Kata Pengantar (kpdi.pdf)
                      <br><a href="#" class="read-more">Download</a>
      </p>
      <p class="des2">
        <b>I. Daftar Isi (daftarisi.pdf)
          <br><a href="#" class="read-more">Download</a>
          <br><b>J. Daftar Gambar (daftargambar.pdf)
            <br><a href="#" class="read-more">Download</a>
            <br><b>K. Daftar Tabel (daftartabel.pdf)
              <br><a href="#" class="read-more">Download</a>
              <br><b>N. Daftar Lampiran (daftarlampiran.pdf)
                <br><a href="#" class="read-more">Download</a>
                <br><b>O. BAB 1 (bab1.pdf)
                  <br><a href="#" class="read-more">Download</a>
                  <br><b>P. BAB 2 (bab2.pdf)
                    <br><a href="#" class="read-more">Download</a>
                    <br><b>Q. BAB 3 (bab3.pdf)
                      <br><a href="#" class="read-more">Download</a>
                      <br><b>R. BAB 4 (bab4.pdf)
                        <br><a href="#" class="read-more">Download</a>
                        <br><b>S. BAB 5 (bab5.pdf)
                          <br><a href="#" class="read-more">Download</a>
                          <br><b>X. Daftar Pustaka (dp.pdf)
                            <br><a href="#" class="read-more">Download</a>
      </p>
      <!-- Awal Footer -->
      <footer>
        <div class="container">
          <div class="row">
            <div class="col">
              <a href="">
                <img src="/img/erp-logo.png" alt="ERP Laboratory" />
              </a>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <p style="color: white; margin-bottom: 0px;">C226 Building Telkom Engineering School,
                Jl.Telekomunikasi No. 1 Terusan Buah Batu <br> Dayeuh Kolot, Bandung</p>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <a href="">
                <img src="/img/social/pin.png" alt="" style="margin-right: 10px;" />
              </a>
              <a href="">
                <img src="/img/social/ig.png" alt="" style="margin-right: 10px;" />
              </a>
              <a href="">
                <img src="/img/social/yt.png" alt="" style="margin-right: 10px;" />
              </a>
              <a href="">
                <img src="/img/social/fb.png" alt="" style="margin-right: 10px;" />
              </a>
              <a href="">
                <img src="/img/social/li.png" alt="" style="margin-right: 10px;" />
              </a>
            </div>
          </div>
          <div class="row border-top mt-3">
            <div class="col">
              <p style="color: white; margin-bottom: 0px;">2020 ERP Laboratory<br>Managed By Public Relation ERP
                Lab | Developed By</p>
            </div>
          </div>
        </div>
      </footer>
      <!-- Akhir Footer -->

      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="/js/jquery-3.5.1.min.js"></script>
      <script src="/js/popper.min.js"></script>
      <script src="/js/bootstrap.js"></script>
      <script src="/js/all.js"></script>
      <script>
        $(document).ready(() => {
          $('.card[data-clickable=true]').click(function(e) {
            var href = $(e.currentTarget).data('href')
            console.log(href)
            window.location = href
          })
        })
      </script>
</body>

</html>