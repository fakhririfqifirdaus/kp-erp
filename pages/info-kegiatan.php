<?php
include '../config_database.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.css" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="/css/all.css" />

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet" />

    <!-- My CSS -->
    <link rel="stylesheet" href="/css/style.css" />

    <title>ERP Laboratory</title>
</head>

<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="/index.php">
                <img src="/img/erp-logo.png" alt="ERP Laboratory" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav text-uppercase mx-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/pages/tentang-kami.php">Tentang Kami</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Event
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="/pages/event-berita.php">Berita Terbaru</a>
                            <a class="dropdown-item active" href="/pages/info-kegiatan.php">Info Kegiatan</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Repository
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Unggah Abstrak</a>
                            <a class="dropdown-item" href="#">Database Alumni</a>
                            <a class="dropdown-item" href="#">Cek Plagiarisme</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Lowongan</a>
                    </li>
                </ul>
                <a href="" class="nav-link">MASUK</a>
                <a href="" class="nav-link">DAFTAR</a>
            </div>
        </div>
    </nav>
    <!-- Akhir Navbar -->
    <!--Main Content  -->
    <div class="header">
        <div class="container mt-2">
            <h3>Info Kegiatan</h3>
        </div>
    </div>

    <!--info kegiatan-->
    <div class="container">
        <div class="row">
            <?php
            $batas   = 6;
            $halaman = @$_GET['page'];
            if (empty($halaman)) {
                $posisi  = 0;
                $halaman = 1;
            } else {
                $posisi  = ($halaman - 1) * $batas;
            }
            $sql = "SELECT * FROM kegiatan ORDER BY tanggal desc limit $posisi,$batas";
            $pagination = mysqli_query($koneksi, $sql);
            while ($kegiatan = mysqli_fetch_array($pagination)) {
            ?>
                <div class="col-lg-3 d-flex mr-5 mt-3">

                    <div class="shadow p-3 mb-5 bg-white rounded-lg ">

                        <img src="/img/<?= $kegiatan['img'] ?>" class="card-img-top" alt="foto">
                        <div class=" card-body ">
                            <h6 class="card-title"><?= $kegiatan['judul'] ?></h6>
                            <h10 class="card-subtitle"><?= $kegiatan['deskripsi'] ?></h10>
                            <br></br>
                            <a href="info-kegiatan-detail.php" class="text-info">Baca Selengkapnya</a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <!--akhir info kegiatan-->
    <!-- Page Ignation -->
    <!--button next-->
    <div class="container">
        <nav aria-label="Page navigation">
            <ul class="pagination justify-content-center ">
                <!-- LINK FIRST AND PREV -->
                <?php
                if ($halaman == 1) { // Jika page adalah page ke 1, maka disable link PREV
                ?>
                    <li class="page-item disabled"><a class="page-link" href="#">First</a></li>
                    <li class="page-item disabled"><a class="page-link" href="#">&laquo;</a></li>
                <?php
                } else { // Jika page bukan page ke 1
                    $link_prev = ($halaman > 1) ? $halaman - 1 : 1;
                ?>
                    <li class="page-item"><a class="page-link" href="?page=1">First</a></li>
                    <li class="page-item"><a class="page-link" href="?page=<?php echo $link_prev; ?>">&laquo;</a>
                    </li>
                <?php
                }
                ?>

                <!-- LINK NUMBER -->
                <?php
                // Buat query untuk menghitung semua jumlah data
                $query2 = mysqli_query($koneksi, "select * from kegiatan");
                $jmldata = mysqli_num_rows($query2);
                $jmlhalaman = ceil($jmldata / $batas);
                $range = 3;
                $awal = ($halaman > $range) ? $halaman - $range : 1;
                $akhir = ($halaman < ($jmlhalaman - $range)) ? $halaman + $range : $jmlhalaman;

                for ($i = $awal; $i <= $akhir; $i++) {
                    $link_active = ($halaman == $i) ? ' class="page-item active"' : 'class="page-item"';
                ?>
                    <li <?php echo $link_active; ?>><a class="page-link" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                <?php
                }
                ?>

                <!-- LINK NEXT AND LAST -->
                <?php
                // Jika page sama dengan jumlah page, maka disable link NEXT nya
                // Artinya page tersebut adalah page terakhir 
                if ($halaman == $jmlhalaman) { // Jika page terakhir
                ?>
                    <li class="page-item disabled"><a class="page-link" href="#">&raquo;</a></li>
                    <li class="disabled"><a class="page-link" href="#">Last</a></li>
                <?php
                } else { // Jika Bukan page terakhir
                    $link_next = ($halaman < $jmlhalaman) ? $halaman + 1 : $jmlhalaman;
                ?>
                    <li class="page-item"><a class="page-link" href="?page=<?php echo $link_next; ?>">&raquo;</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="?page=<?php echo $jmlhalaman; ?>">Last</a>
                    </li>
                <?php
                }
                ?>
            </ul>
        </nav>
    </div>
    <!-- Akhir Main Content -->
    <!-- Awal Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="">
                        <img src="/img/erp-logo.png" alt="ERP Laboratory" />
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p style="color: white; margin-bottom: 0px;">C226 Building Telkom Engineering School,
                        Jl.Telekomunikasi No. 1 Terusan Buah Batu <br> Dayeuh Kolot, Bandung</p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <a href="">
                        <img src="/img/pin.png" alt="" style="margin-right: 10px;" />
                    </a>
                    <a href="">
                        <img src="/img/ig.png" alt="" style="margin-right: 10px;" />
                    </a>
                    <a href="">
                        <img src="/img/yt.png" alt="" style="margin-right: 10px;" />
                    </a>
                    <a href="">
                        <img src="/img/fb.png" alt="" style="margin-right: 10px;" />
                    </a>
                    <a href="">
                        <img src="/img/li.png" alt="" style="margin-right: 10px;" />
                    </a>
                </div>
            </div>
            <div class="row border-top mt-3">
                <div class="col">
                    <p style="color: white; margin-bottom: 0px;">2020 ERP Laboratory<br>Managed By Public Relation ERP
                        Lab | Developed By</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Akhir Footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/js/jquery-3.5.1.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/all.js"></script>
    <script>
        $(document).ready(() => {
            $('.card[data-clickable=true]').click(function(e) {
                var href = $(e.currentTarget).data('href')
                console.log(href)
                window.location = href
            })
        })
    </script>
</body>

</html>