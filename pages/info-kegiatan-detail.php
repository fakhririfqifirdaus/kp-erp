<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.css" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="/css/all.css" />

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet" />

    <!-- My CSS -->
    <link rel="stylesheet" href="/css/style.css" />

    <title>ERP Laboratory</title>
</head>

<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="/index.php">
                <img src="/img/erp-logo.png" alt="ERP Laboratory" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav text-uppercase mx-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/pages/tentang-kami.php">Tentang Kami</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Event
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="/pages/event-berita.php">Berita Terbaru</a>
                            <a class="dropdown-item active" href="/pages/info-kegiatan.php">Info Kegiatan</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Repository
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Unggah Abstrak</a>
                            <a class="dropdown-item" href="#">Database Alumni</a>
                            <a class="dropdown-item" href="#">Cek Plagiarisme</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Lowongan</a>
                    </li>
                </ul>
                <a href="" class="nav-link">MASUK</a>
                <a href="" class="nav-link">DAFTAR</a>
            </div>
        </div>
    </nav>
    <!-- Akhir Navbar -->

    <!-- Awal Main Content -->
    <div class="header">
        <div class="container my-5">
            <h3>Info Kegiatan</h3>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h3><b>ERP LABORATORY ON BANDUNG ICT EXPO 2018</b></h3>
                        <img src="/img/ict_bandung.png" class="card-img-top" alt="foto">
                        <p></p>
                        <p>
                            Laboratorium ERP menjadi salah satu Exhibitor di Bandung ICT EXPO 2018, DIGITAL BROADBAND
                            SUMMIT yang diselenggarakan di Trans Convention Center

                            ia
                            #ForTheFuture
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h4><b>Aktivitas Lainnya</b></h4>
                        <div class="card" style="width: 18rem;">
                            <img src="/img/public lecture.png" class="card-img-top" alt="foto">
                            <div class="card-body">
                                <h5 class="card-title">Public lecture</h5>
                                <p class="card-text">Attention to all 2016 & 2017 Information System students who.…</p>
                                <a href="#"><u> Selengkapnya</u> </a>
                            </div>
                        </div>
                        <p></p>
                        <p></p>
                        <div class="card" style="width: 18rem;">
                            <img src="/img/makrab.png" class="card-img-top" alt="foto">
                            <div class="card-body">
                                <h5 class="card-title">Pengumuman Malam Keakraban</h5>
                                <p class="card-text">Makrab merupakan kegiatan internal yang diadakan Lab ERP untuk..
                                </p>
                                <a href="#"><u> Selengkapnya</u> </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Akhir Main Content  -->

    <!-- Awal Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="">
                        <img src="/img/erp-logo.png" alt="ERP Laboratory" />
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p style="color: white; margin-bottom: 0px;">C226 Building Telkom Engineering School,
                        Jl.Telekomunikasi No. 1 Terusan Buah Batu <br> Dayeuh Kolot, Bandung</p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <a href="">
                        <img src="/img/pin.png" alt="" style="margin-right: 10px;" />
                    </a>
                    <a href="">
                        <img src="/img/ig.png" alt="" style="margin-right: 10px;" />
                    </a>
                    <a href="">
                        <img src="/img/yt.png" alt="" style="margin-right: 10px;" />
                    </a>
                    <a href="">
                        <img src="/img/fb.png" alt="" style="margin-right: 10px;" />
                    </a>
                    <a href="">
                        <img src="/img/li.png" alt="" style="margin-right: 10px;" />
                    </a>
                </div>
            </div>
            <div class="row border-top mt-3">
                <div class="col">
                    <p style="color: white; margin-bottom: 0px;">2020 ERP Laboratory<br>Managed By Public Relation ERP
                        Lab | Developed By</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Akhir Footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/js/jquery-3.5.1.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/all.js"></script>
    <script>
        $(document).ready(() => {
            $('.card[data-clickable=true]').click(function(e) {
                var href = $(e.currentTarget).data('href')
                console.log(href)
                window.location = href
            })
        })
    </script>
</body>

</html>