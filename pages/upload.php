<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <title>Upload</title>
</head>

<body>
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="/index.html">
        <img src="/img/erp-logo.png" alt="ERP Laboratory" />
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav text-uppercase mx-auto">
          <li class="nav-item">
            <a class="nav-link" href="/pages/tentang-kami.html">Tentang Kami</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Event
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="/event/berita-terbaru.html">Berita Terbaru</a>
              <a class="dropdown-item" href="/event/info-kegiatan.html">Info Kegiatan</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Repository
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="unggah-abstrak.html">Unggah Abstrak</a>
              <a class="dropdown-item" href="database-alumni.html">Database Alumni</a>
              <a class="dropdown-item" href="cek-plagiarisme.html">Cek Plagiarisme</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="lowongan.html">Lowongan</a>
          </li>
        </ul>
        <a href="" class="nav-link">MASUK</a>
        <a href="" class="nav-link">DAFTAR</a>
      </div>
    </div>
  </nav>
  <!-- Akhir Navbar -->

  <form action="prosesupload.php" method="POST" enctype="multipart/form-data">
    <div class="form-group">
      <label for="exampleFormControlInput1">Judul</label>
      <input type="text" class="form-control" name="judul" id="exampleFormControlInput1" placeholder="Masukkan Judul">
    </div>
    <div class="form-group">
      <label for="exampleFormControlInput1">Tahun</label>
      <input type="text" class="form-control" name="tahun" id="exampleFormControlInput1" placeholder="Masukkan Tahun">
    </div>
    <div class="form-group">
      <label for="exampleFormControlInput1">Pengarang</label>
      <input type="text" class="form-control" name="pengarang" id="exampleFormControlInput1" placeholder="Masukkan Nama Pengarang">
    </div>
    <div class="form-group">
      <label for="exampleFormControlInput1">Penerbit</label>
      <input type="text" class="form-control" name="penerbit" id="exampleFormControlInput1" placeholder="Masukkan Penerbit">
    </div>
    <div class="form-group">
      <label for="exampleFormControlFile1">PDF File</label>
      <input type="file" class="form-control-file" name="pdf_file" id="exampleFormControlFile1">
    </div>
    <div class="form-group">
      <label for="exampleFormControlFile1">Cover</label>
      <input type="file" class="form-control-file" name="cover" id="exampleFormControlFile1">
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>

  </form>
  <!-- Awal Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col">
          <a href="">
            <img src="/img/erp-logo.png" alt="ERP Laboratory" />
          </a>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <p style="color: white; margin-bottom: 0px;">C226 Building Telkom Engineering School,
            Jl.Telekomunikasi No. 1 Terusan Buah Batu <br> Dayeuh Kolot, Bandung</p>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <a href="">
            <img src="/img/social/pin.png" alt="" style="margin-right: 10px;" />
          </a>
          <a href="">
            <img src="/img/social/ig.png" alt="" style="margin-right: 10px;" />
          </a>
          <a href="">
            <img src="/img/social/yt.png" alt="" style="margin-right: 10px;" />
          </a>
          <a href="">
            <img src="/img/social/fb.png" alt="" style="margin-right: 10px;" />
          </a>
          <a href="">
            <img src="/img/social/li.png" alt="" style="margin-right: 10px;" />
          </a>
        </div>
      </div>
      <div class="row border-top mt-3">
        <div class="col">
          <p style="color: white; margin-bottom: 0px;">2020 ERP Laboratory<br>Managed By Public Relation ERP
            Lab | Developed By</p>
        </div>
      </div>
    </div>
  </footer>
  <!-- Akhir Footer -->

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="/js/jquery-3.5.1.min.js"></script>
  <script src="/js/popper.min.js"></script>
  <script src="/js/bootstrap.js"></script>
  <script src="/js/all.js"></script>
  <script>
    $(document).ready(() => {
      $('.card[data-clickable=true]').click(function(e) {
        var href = $(e.currentTarget).data('href')
        console.log(href)
        window.location = href
      })
    })
  </script>
</body>

</html>