<?php

include 'config_database.php';


// filter data yang diinputkan
$judul = filter_input(INPUT_POST, 'judul', FILTER_SANITIZE_STRING);
$tahun = filter_input(INPUT_POST, 'tahun', FILTER_SANITIZE_STRING);
$pengarang = filter_input(INPUT_POST, 'pengarang', FILTER_SANITIZE_STRING);
$penerbit = filter_input(INPUT_POST, 'penerbit', FILTER_SANITIZE_STRING);

//file
$pdf_file = $_FILES['pdf_file']['name'];
$cover = $_FILES['cover']['name'];

if ($pdf_file != "" || $cover != "") {

  $ekstensi_diperbolehkan = array('pdf', 'jpg', 'png'); //ekstensi file yang bisa diupload 
  $x = explode('.', $pdf_file); //memisahkan nama file dengan ekstensi yang diupload
  $x_gambar = explode('.', $cover); //memisahkan nama file dengan ekstensi yang diupload
  $ekstensi = strtolower(end($x));
  $ekstensi_gambar = strtolower(end($x_gambar));
  $file_tmp = $_FILES['pdf_file']['tmp_name'];
  $file_tmp_cover = $_FILES['cover']['tmp_name'];
  $angka_acak     = rand(1, 999);
  $nama_pdf = $angka_acak . '-' . $pdf_file; //menggabungkan angka acak dengan nama file sebenarnya
  $nama_gambar = $angka_acak . '-' . $cover; //menggabungkan angka acak dengan nama file sebenarnya

  if (in_array($ekstensi,  $ekstensi_diperbolehkan) === true && in_array($ekstensi_gambar, $ekstensi_diperbolehkan) === true) {
    move_uploaded_file($file_tmp, 'upload/' . $nama_pdf); //memindah file pdf ke folder upload
    move_uploaded_file($file_tmp_cover, 'gambar/' . $nama_gambar); //memindah file pdf ke folder upload

    $query = "INSERT INTO lowongan (judul, tahun, pengarang, penerbit, pdf_file, gambar) 
        VALUES ('$judul', '$tahun', '$pengarang', '$penerbit', '$nama_pdf', '$nama_gambar')";
    $result = mysqli_query($koneksi, $query);
    // periska query apakah ada error
    if (!$result) {
      die("Query gagal dijalankan: " . mysqli_errno($koneksi) .
        " - " . mysqli_error($koneksi));
    } else {
      //tampil alert dan akan redirect ke halaman index.php
      //silahkan ganti index.php sesuai halaman yang akan dituju
      echo "<script>alert('Data berhasil ditambah.');window.location='upload.php';</script>";
    }
  } else {
    //jika file ekstensi tidak jpg dan png maka alert ini yang tampil
    echo "<script>alert('Ekstensi file yang boleh hanya pdf.');window.location='upload.php';</script>";
  }
} else {
  echo "<script>alert('Pastikan semua data terisi.');</script>";
}
