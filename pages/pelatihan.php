<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.css" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="/css/all.css" />

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap"
        rel="stylesheet" />

    <!-- My CSS -->
    <link rel="stylesheet" href="/css/style.css" />

    <title>ERP Laboratory</title>
</head>

<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light">
		<div class="container">
			<a class="navbar-brand" href="/index.php">
				<img src="/img/erp-logo.png" alt="ERP Laboratory" />
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
				aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavDropdown">
				<ul class="navbar-nav text-uppercase mx-auto">
					<li class="nav-item">
						<a class="nav-link" href="/pages/tentang-kami.php">Tentang Kami</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Event
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="/pages/event-berita.php">Berita Terbaru</a>
							<a class="dropdown-item" href="/pages/info-kegiatan.php">Info Kegiatan</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Repository
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="#">Unggah Abstrak</a>
							<a class="dropdown-item" href="#">Database Alumni</a>
							<a class="dropdown-item" href="#">Cek Plagiarisme</a>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Lowongan</a>
					</li>
				</ul>
				<a href="" class="nav-link">MASUK</a>
				<a href="" class="nav-link">DAFTAR</a>
			</div>
		</div>
	</nav>
    <!-- Akhir Navbar -->

    <!--Main Content  -->
    <!-- Main Content 1 -->
    <div class="container my-5">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="about-info">
                    <div class="section-title wow fadeInUp animated" data-wow-delay="0.2s"
                        style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <h3><strong>Pelatihan dan Sertifikasi</strong></h3>
                    <br>
                    </div>

                    <class class="wow fadeInUp animated" data-wow-delay="0.4s"
                        style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                        <p align="justify">Sebagai bagian akademik yang berperan untuk mengembangkan pengetahuan ERP
                            entitas Fakultas Rekayasa Industri, kami juga menyediakan layanan pelatihan dan sertifikasi
                            ERP. Adapun layanan sertifikasi yang kami sediakan adalah :</p>

                        <ol>
                            <li><strong>SAP SCM500 (Business Processes in Procurement)</strong></li>
                            <li><strong>SAP SCM600 (Business Processes in Sales Order Management)</strong></li>
                            <li><strong>SAP PLM200 (Project Management)</strong></li>
                            <li><strong>SAP ADM100 (Web As Administration)</strong></li>
                            <li><strong>SAP BC400 (Introduction to ABAP Workbench)</strong></li>
                            <li><strong>SAP AC040 (Business Process in Financial Controlling)</strong></li>
                        </ol>

                        <p align="justify">
                            <strong>PORTFOLIO TRAINING</strong>

                            <ol>
                                <li>Pelatihan SAP01 untuk Dosen Universitas Negeri Padang dan Dinas Perindustrian dan
                                    perdangangan jawa barat (2018)</li>
                                <li>Pelatihan SCM500 Untuk Mahasiswa Kelas Akselerasi (2019)</li>
                                <li>Pelatihan BC400 Untuk Mahsiswa Sistem Informasi (2019)</li>
                                <li>Pelatihan HR050 Untuk Asisten Praktikum Manajemen SDM (2019)</li>
                                <li>Pelatihan Odoo ERP untuk Asisten ERP (2019)</li>
                                <li>Pelatihan SCM500 Untuk Dosen Universitas Telkom (2020)</li>
                            </ol>
                            <br>
                        </p>
                    </class>
                </div>
            </div>
        </div>
    </div>
    <!-- Akhir Main Content 1 -->
    <!-- Akhir Main Content -->

    <!-- Awal Footer -->
    <footer>
		<div class="container">
			<div class="row">
				<div class="col">
					<a href="">
						<img src="/img/erp-logo.png" alt="ERP Laboratory" />
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<p style="color: white; margin-bottom: 0px;">C226 Building Telkom Engineering School,
						Jl.Telekomunikasi No. 1 Terusan Buah Batu <br> Dayeuh Kolot, Bandung</p>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<a href="">
						<img src="/img/pin.png" alt="" style="margin-right: 10px;" />
					</a>
					<a href="">
						<img src="/img/ig.png" alt="" style="margin-right: 10px;" />
					</a>
					<a href="">
						<img src="/img/yt.png" alt="" style="margin-right: 10px;" />
					</a>
					<a href="">
						<img src="/img/fb.png" alt="" style="margin-right: 10px;" />
					</a>
					<a href="">
						<img src="/img/li.png" alt="" style="margin-right: 10px;" />
					</a>
				</div>
			</div>
			<div class="row border-top mt-3">
				<div class="col">
					<p style="color: white; margin-bottom: 0px;">2020 ERP Laboratory<br>Managed By Public Relation ERP
						Lab | Developed By</p>
				</div>
			</div>
		</div>
	</footer>
    <!-- Akhir Footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/js/jquery-3.5.1.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/all.js"></script>
    <script>
        $(document).ready(() => {
            $('.card[data-clickable=true]').click(function (e) {
                var href = $(e.currentTarget).data('href')
                console.log(href)
                window.location = href
            })
        })
    </script>
</body>

</html>